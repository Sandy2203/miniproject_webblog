package de.academy.diedreierdnuesse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiedreierdnuesseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiedreierdnuesseApplication.class, args);
	}
}
