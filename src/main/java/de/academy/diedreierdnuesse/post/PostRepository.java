package de.academy.diedreierdnuesse.post;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByOrderByCreationDateTimeDesc();

    Post findPostById(long id);


}
